package com.jgdy4.ptaki;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Ptak ptak = new Bocian();
        Ptak ptak1 = new Kukulka();

        List<Ptak> lista= new ArrayList<>();
        lista.add(ptak);
        lista.add(ptak1);

        for (Ptak ptak2 : lista) {
            ptak2.spiewaj();
        }

        // kle kle
        // ku ku

    }
}
