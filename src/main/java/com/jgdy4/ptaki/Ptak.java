package com.jgdy4.ptaki;

import java.util.Objects;

public class Ptak {
    private String imie;
    private String nazwisko;

    public void spiewaj(){
        System.out.println("cwir cwir");
    }

    public void spiewaj(String coZaspiewac){
        System.out.println(coZaspiewac);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ptak ptak = (Ptak) o;
        return Objects.equals(imie, ptak.imie) &&
                Objects.equals(nazwisko, ptak.nazwisko);
    }

    @Override
    public int hashCode() {
        return Objects.hash(imie, nazwisko);
    }
}
