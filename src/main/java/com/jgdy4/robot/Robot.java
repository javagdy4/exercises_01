package com.jgdy4.robot;

public class Robot {
    private int poziomBaterii = 100;
    private String nazwa;
    private boolean wlaczony = false;

    public Robot(String nazwa) {
        this.nazwa = nazwa;
    }

    public void poruszRobotem(RuchRobota ruch) {
        if (!wlaczony) {
            System.err.println("Robot jest wyłączony.");
            return;
        }
        if (poziomBaterii >= ruch.getZuzycieEnergii()) {
            System.out.println("Wykonuję ruch robota: " + ruch);
            poziomBaterii -= ruch.getZuzycieEnergii();
        }else{
            System.err.println("Niewystarczająca ilość energii.");
        }
    }

    public void naladuj() {
        if (wlaczony) {
            // nie moze sie ladowac
            System.err.println("Nie można ładować robota gdy jest włączony.");
        } else {
            poziomBaterii = 100;
            System.out.println("Robot naładowany");
        }
    }

    public void włącz() {
        if (wlaczony) {
            System.err.println("Robot jest już włączony.");
        } else {
            wlaczony = true;
            System.out.println("Włączam robota.");
        }
    }

    public void wyłącz() {
        if (!wlaczony) {
            System.err.println("Robot jest już wyłączony.");
        } else {
            wlaczony = false;
            System.out.println("Wyłączam robota.");
        }
    }


}
