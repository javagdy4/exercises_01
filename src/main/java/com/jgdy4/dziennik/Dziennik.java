package com.jgdy4.dziennik;

import java.util.*;
import java.util.stream.Collectors;

public class Dziennik {
    private List<Student> list = new ArrayList<>();

    public void dodajStudenta(Student s) {
        list.add(s);
    }

    public void usunStudentaPoNumerzeIndeksu(Student s) {
        list.remove(s);
    }

    public void usunStudentaPoNumerzeIndeksu(String indeks) {
        for (Student student : list) {
            if (student.getNrIndeksu().equalsIgnoreCase(indeks)) {
                list.remove(student);
                break;
            }
        }

//         opcja 2
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i).getNrIndeksu().equalsIgnoreCase(indeks)) {
//                list.remove(i);
//                i--; // cofnięcie się po usunięciu
//            }
//        }
//
//         opcja 3
//        Iterator<Student> it = list.iterator();
//        while (it.hasNext()){
//            Student st = it.next();
//            if(st.getNrIndeksu().equalsIgnoreCase(indeks)){
//                it.remove();
//            }
//        }
    }

    /**
     * Zwrca studenta lub jeśli nie uda się go odnaleźć to zwraca null.
     *
     * @param indeks - numer indeksu szukanego studenta.
     * @return - Optional z opakowanym studentem.
     */
    public Optional<Student> zwrocStudenta(String indeks) {
        for (Student student : list) {
            if (student.getNrIndeksu().equalsIgnoreCase(indeks)) {
                return Optional.of(student);
            }
        }
        return Optional.empty();
    }


    // Optional<Double>
    public Optional<Double> podajSredniaStudenta(String indeks) {
        Optional<Student> studentOptional = zwrocStudenta(indeks);
        if (studentOptional.isPresent()) {
            Student st = studentOptional.get();

            return Optional.of(st.obliczSrednia());
        }
        return Optional.empty();
    }

    public List<Student> zwrocStudentowZagrozonych() {
        return list.stream()
                .filter(student -> student.obliczSrednia() <= 2.5)
                .collect(Collectors.toList());
    }

    public List<Student> posortujStudentow() {
        Collections.sort(list, new Comparator<Student>() {
            // -1
            // 0
            //  1 --
            @Override
            public int compare(Student student1, Student student2) {
                Long s1 = Long.parseLong(student1.getNrIndeksu());
                Long s2 = Long.parseLong(student2.getNrIndeksu());

                return s2.compareTo(s1); // -1 0 1
            }
        });
        return list;
    }

    public void dodajOcene(String indeks, Double ocena) {
        Optional<Student> studentOptional = zwrocStudenta(indeks);
        if (studentOptional.isPresent()) {
            Student st = studentOptional.get();

            st.getOceny().add(ocena);
        } else {
            throw new RuntimeException("Nie udało się odnaleźć studenta!");
        }
    }
}
