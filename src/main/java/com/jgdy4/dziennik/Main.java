package com.jgdy4.dziennik;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {

        Dziennik d = new Dziennik();
        d.dodajStudenta(new Student("123", "Pawel", "Gawel"));
        d.dodajStudenta(new Student("124", "Pawelek", "Gawelek"));
        d.dodajStudenta(new Student("125", "welek", "Gawelek"));
        d.dodajStudenta(new Student("126", "lek", "Gawel"));
        d.dodajStudenta(new Student("127", "elek", "Gawelk"));

//        d.usunStudentaPoNumerzeIndeksu("124");
//        d.usunStudentaPoNumerzeIndeksu("125");

        Optional<Student> pudelko = d.zwrocStudenta("124");
        if(pudelko.isPresent()) {
            Student s = pudelko.get();

            System.out.println(s.getImie());
        }

        d.posortujStudentow().forEach(s -> System.out.println(s));
        d.posortujStudentow().forEach(System.out::println);
    }
}
