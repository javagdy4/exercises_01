package com.jgdy4.dziennik;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

@Data
@Getter
@Setter
@NoArgsConstructor
public class Student {
    private List<Double> oceny = new ArrayList<>();
    private String nrIndeksu;
    private String imie;
    private String nazwisko;

    public Student(String nrIndeksu, String imie, String nazwisko) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Double obliczSrednia() {
        double suma = 0.0;

        // forma 1
        for (int i = 0; i < oceny.size(); i++) {
            suma += oceny.get(i);
        }
        // forma 2
//        suma = oceny.stream().mapToDouble(Double::doubleValue).sum();

        // forma 3 (od razu daje wynik);
//        Optional<Double>
        OptionalDouble srednia = oceny.stream().mapToDouble(Double::doubleValue).average();
        return srednia.orElse(0.0);
//        return suma / oceny.size();
    }
}
